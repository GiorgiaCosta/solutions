import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    file = open(inputFilePath, 'r')
    text = file.readline()  # \ legge riga carattere per carattere

    file.close()
    return True, text

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    encryptedText:  str = ""
    j: int = 0
    n: int = 0

    # \ i da 0(incluso) a n (escluso) con range
    for i in range(0, len(text)):
        if text[i].isalnum() or text[i] == " ":
            j = i - (len(password)) * int(i / len(password))

            if password[j].isupper():
                n = ord(text[i]) + ord(password[j])
                if n > 126:                                                # torno al 32
                    n -= 94

                encryptedText += chr(n)

            else:
                print("Password non contiene solo lettere maiuscole!")

        else:
            print("Caratteri non adeguati nella stringa passata text in posizione ", i)

    return True, encryptedText

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    decryptedText: str = ""
    n: int = 0
    j: int = 0

    for i in range(0, len(text)):
        j = i - (len(password)) * int(i / len(password))

        if password[j].isupper():
            n = ord(text[i]) - ord(password[j])

            if n < 32:
                n += 94

            decryptedText += chr(n)

        else:
                print("Password non contiene solo lettere maiuscole in posizione!")

    return True, decryptedText


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)