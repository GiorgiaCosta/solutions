#include <iostream>
#include <fstream>
#include <sstream>
#include <string> // aggiunta per dichiarazione variabile

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)  //numero argomenti di comando è un intero, il valore è un char pointer pointer cioe puntatore a vettore di char, vettore di char pointer cioe vettore di stringhe
//in posizione 1 c'è nome di esegiobile
{
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{
    ifstream file;   //lettura file
    file.open(inputFilePath);

    getline(file,text);

    file.close();
  return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)

{
    int intero_encrypt;
    int j = 0;
    encryptedText.resize(text.size());

    for (unsigned int i = 0; i < text.size(); i++){

        if(std::isalnum(text[i]) || text[i] == ' '){
             j = i - password.size() * (i / password.size());  //indice della stringa password riletta da capo

             if(isupper(password[j])){
                 intero_encrypt = int(text[i]) + int(password[j]);

                    if(intero_encrypt > 126){
                        intero_encrypt -= 94;
                    }

                    encryptedText[i] = char(intero_encrypt);

             } else{
                 cout<<"Password non contiene solo lettere maiuscole!"<<endl;
             }

        } else {
            cout<<"Carattere non adeguati in lettura in posizione "<<i<<endl;
        }
        }

  return true;
}

bool Decrypt(const string& text,        //la stringa text passata è encryptedText
             const string& password,
             string& decryptedText)
{
    int intero_decrypt;
    int j = 0;
    decryptedText.resize(text.size());

    for (unsigned int i = 0; i <= text.size() - 1; i++){


             j = i - password.size() * (i / password.size());

             if(isupper(password[j])){
                 intero_decrypt = int(text[i]) - int(password[j]);

                    if(intero_decrypt < 32){
                        intero_decrypt += 94;
                    }

                    decryptedText[i] = char(intero_decrypt);

             } else{
                 cout<<"Password non contiene solo lettere maiuscole!"<<endl;
             }
}

  return true;
}
