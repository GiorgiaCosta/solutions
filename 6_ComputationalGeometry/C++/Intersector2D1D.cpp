#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D1D::NoIntersection;

}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    _planeTranslation = planeTranslation;
    _planeNormal = planeNormal;

}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    _lineOrigin = lineOrigin;
    _lineTangent =  lineTangent;

}


Vector3d Intersector2D1D::IntersectionPoint()
{
                         /// CALCOLO COORDINATA PARAMETRICA ///
    double dot = 0;
    double dot1 = 0;
    double dot2 = 0;

    for(int i=0; i < 3; i++)
        dot += _planeNormal(i) * _planeTranslation;   // vettore normale(trasposto) * x0

    for(int i=0; i < 3; i++)
        dot1 += _planeNormal(i) * _lineOrigin(i);   // vettore normale(trasposto) * y0

    for(int i=0; i < 3; i++)
        dot2 += _planeNormal(i) * _lineTangent(i);   //vettore normale(trasposto) * t

    intersectionParametricCoordinate = (dot - dot1) / dot2;

                          /// CALCOLO PUNTO DI INTERSEZIONE///

    Vector3d intersectionPoint, ver2;
    for(int i=0; i < 3; i++)
        ver2(i) = intersectionParametricCoordinate * _lineTangent(i);
    for(int i=0; i<3; i++)
        intersectionPoint(i) = _lineOrigin(i) + ver2(i);

    return intersectionPoint;
}


// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    double dot = 0;
    double dot1 = 0;
    double dot2 = 0;

    for(int i=0; i < 3; i++)
        dot += _planeNormal(i) * _planeTranslation;

    for(int i=0; i < 3; i++)
        dot1 += _planeNormal(i) * _lineOrigin(i);

    for(int i=0; i < 3; i++)
        dot2 += _planeNormal(i) * _lineTangent(i);


    intersectionType = NoIntersection;
    bool intersection = false;

    double numeratore;
    double denominatore;
    numeratore = dot - dot1;
    denominatore = dot2;

    if(abs(denominatore) >  toleranceIntersection){ //INCIDENTE
            intersectionType = PointIntersection;
            intersection = true;
    }

    if(abs(denominatore) <  toleranceIntersection){

        if(abs(numeratore) > toleranceParallelism){ //PARALLELA
            intersectionType = NoIntersection;
            }

        if(abs(numeratore) < toleranceParallelism){ //COINCIDENTE = false
            intersectionType = Coplanar;
            }
    }

  return intersection;
}
