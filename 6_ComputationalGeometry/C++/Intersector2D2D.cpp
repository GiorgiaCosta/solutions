#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1.0E-5;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D2D::NoInteresection;
}


Intersector2D2D::~Intersector2D2D()
{

}


void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    rightHandSide(0) = planeTranslation;

    for(int i=0; i<3; i++)
        matrixNomalVector(0,i) = planeNormal(i);

}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    rightHandSide(1) = planeTranslation;

    for(int i=0; i<3; i++)
        matrixNomalVector(1,i) = planeNormal(i);
}



// ***************************************************************************

bool Intersector2D2D::ComputeIntersection()
{
    bool intersection = false;
    Vector3d n1;
    Vector3d n2;
    Vector3d n3;  //prodotto vettoriale tra n1 e n2

    n3(0) = matrixNomalVector(0,1) * matrixNomalVector(1,2) - matrixNomalVector(0,2) * matrixNomalVector(1,1);
    n3(1) = - (matrixNomalVector(0,0) * matrixNomalVector(1,2) - matrixNomalVector(0,2) * matrixNomalVector(1,0));
    n3(2) = matrixNomalVector(0,0) * matrixNomalVector(1,1) - matrixNomalVector(0,1) * matrixNomalVector(1,0);

    if(n3.norm() > toleranceIntersection){
        intersectionType = LineIntersection;
        intersection = true;

    } else{

        for (int i=0; i<3; i++){               //memorizzato in SetFirstPlane
            n1(i) = matrixNomalVector(0, i);
        }
        for (int i=0; i<3; i++){               //memorizzato in SetsecondPlane
            n2(i) = matrixNomalVector(1, i);
        }

        //se differenza tra norma n1 e norma n2 = 0 e le traslazioni dei due piani memorizzate nei set in rightHandSide sono uguali
        if(((n1.norm() - n2.norm()) < toleranceParallelism) && (rightHandSide(0) == rightHandSide(1) )){
            intersectionType = Coplanar;        // coplanari
            intersection = false;
        } else {
            intersectionType = NoInteresection;    //paralleli
            intersection = false;

        }


}
    return intersection;
}
