import math

class Point:
    def __init__(self, x: float, y: float):
        self.x = x  #/inizializzo in attributi
        self.y = y


class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.__center = center
        self.__a = a
        self.__b = b
    def area(self):
        return self.__a * self.__b * math.pi


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super().__init__(center, radius, radius)  # ereditarietà
        self.__center = center
        self.__radius = radius

    def area(self):
        return Ellipse.area(Ellipse(self.__center, self.__radius, self.__radius))




class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.__p1 = p1
        self.__p2 = p2
        self.__p3 = p3

    def area(self):  # formula shoelace o dell'area di Gauss
        return 0.5 * abs(self.__p1.x * self.__p2.y + self.__p2.x * self.__p3.y + self.__p3.x * self.__p1.y - self.__p2.x * self.__p1.y - self.__p3.x * self.__p2.y - self.__p1.x * self.__p3.y)


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, Point(edge+p1.x, p1.y), Point(p1.x+(edge/2), (math.sqrt(3)/2) * edge+p1.y))
        self.__edge = edge
        self.__p1 = p1

    def area(self):
        return Triangle.area(Triangle(self.__p1, Point(self.__edge+self.__p1.x, self.__p1.y), Point(self.__p1.x+(self.__edge/2), (math.sqrt(3)/2) * self.__edge+self.__p1.y)))


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.__p1 = p1
        self.__p2 = p2
        self.__p3 = p3
        self.__p4 = p4

    def area(self):
        return 0.5 * abs(self.__p1.x * self.__p2.y + self.__p2.x * self.__p3.y + self.__p3.x * self.__p4.y + self.__p4.x * self.__p1.y -self.__p2.x * self.__p1.y - self.__p3.x * self.__p2.y - self.__p4.x * self.__p3.y - self.__p1.x * self.__p4.y)


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        super().__init__(p1, p2, Point(p2.x + p4.x - p1.x, p4.y + p2.y - p1.y), p4)
        self.__p1 = p1
        self.__p2 = p2
        self.__p4 = p4

    def area(self):
        return Quadrilateral.area(Quadrilateral(self.__p1, self.__p2, Point(self.__p2.x + self.__p4.x - self.__p1.x, self.__p4.y + self.__p2.y - self.__p1.y), self.__p4))


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        super().__init__(p1, Point(p1.x+base, p1.y), Point(p1.x, p1.y+height))
        self.__p1 = p1
        self.__base = base
        self.__height = height

    def area(self):
        return Parallelogram.area(Parallelogram(self.__p1, Point(self.__p1.x+self.__base, self.__p1.y), Point(self.__p1.x, self.__p1.y+self.__height)))


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, edge, edge)
        self.__p1 = p1
        self.__edge = edge

    def area(self):
        return Rectangle.area(Rectangle(self.__p1, self.__edge, self.__edge))
