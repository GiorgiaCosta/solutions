#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <cmath>


using namespace std;

namespace ShapeLibrary {

  class Point {

  public:
      double _x;
      double _y;

    public:
      Point(const double& x,
            const double& y);

      Point(const Point& point);

      Point(){}
  };


  class IPolygon {
    public:
      virtual double Area() const = 0;   //metodo astratto
  };

  class Ellipse : public IPolygon
  {
  protected:
      Point _center;
      int _a;
      int _b;

    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area() const { return (_a * _b * M_PI); }
  };

  class Circle : public Ellipse
  {
    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const { return Ellipse::Area(); }
  };


  class Triangle : public IPolygon
  {
  private:
      Point _p1, _p2, _p3;

    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

        // formula shoelace o dell'area di Gauss
      double Area() const { return 0.5 * abs(_p1._x*_p2._y + _p2._x*_p3._y + _p3._x*_p1._y - _p2._x*_p1._y - _p3._x*_p2._y - _p1._x*_p3._y); }
  };


  class TriangleEquilateral : public Triangle
  {
  public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const { return Triangle::Area(); }
  };

  class Quadrilateral : public IPolygon
  {
  private:
      Point _p1, _p2, _p3, _p4;

    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

        // formula shoelace o dell'area di Gauss
      double Area() const { return 0.5 * abs(_p1._x*_p2._y + _p2._x*_p3._y + _p3._x*_p4._y + _p4._x*_p1._y - _p2._x*_p1._y - _p3._x*_p2._y - _p4._x*_p3._y - _p1._x*_p4._y) ;};
  };


  class Parallelogram : public Quadrilateral
  {
  public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const { return Quadrilateral::Area();}
  };

  class Rectangle : public Parallelogram
  {
    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const { return Parallelogram::Area(); }
  };

  class Square: public Rectangle
  {
    public:
      Square(const Point& p1,
             const int& edge);

      double Area() const { return Rectangle::Area(); }
  };
}

#endif // SHAPE_H
