#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y){
    _x = x;
    _y = y;
}

Point::Point(const Point &point){
    _x = point._x;
    _y = point._y;
}

Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
    _center._x = center._x;
    _center._y = center._y;
    _a = a;
    _b = b;
}

Circle::Circle(const Point &center, const int &radius) : Ellipse(center, radius, radius)
{

}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    /*_p1 = p1;  se facessi un overload
    _p2 = p2;
    _p3 = p3;*/

    _p1._x = p1._x;
    _p1._y = p1._y;
    _p2._x = p2._x;
    _p2._y = p2._y;
    _p3._x = p3._x;
    _p3._y = p3._y;

}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge) :Triangle(p1, Point(edge+p1._x, p1._y), Point(p1._x+(edge/2), (sqrt(3)/2)*edge+p1._y))
{

}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    _p1._x = p1._x;
    _p1._y = p1._y;
    _p2._x = p2._x;
    _p2._y = p2._y;
    _p3._x = p3._x;
    _p3._y = p3._y;
    _p4._x = p4._x;
    _p4._y = p4._y;


}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4) : Quadrilateral(p1, p2, Point (p2._x + p4._x - p1._x, p4._y + p2._y - p1._y), p4)
{

}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height) : Parallelogram(p1, Point(p1._x+base, p1._y), Point(p1._x, p1._y+height))
{

}

Square::Square(const Point &p1, const int &edge) : Rectangle(p1, edge, edge)
{

}



}
