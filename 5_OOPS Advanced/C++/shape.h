#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point(): X(0),Y(0) { }
      Point(const double& x,
            const double& y): X(x), Y(y) { }
      Point(const Point& point) {
          X = point.X;
          Y = point.Y;
      }

      double ComputeNorm2() const { return sqrt(X*X+Y*Y); }

      //overload
      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);

      friend ostream& operator<<(ostream& stream, const Point& point) //amicizia non transitiva
      {
        stream << "Point: x = " << point.X << " y = " << point.Y << endl;
        return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;

      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() < rhs.Perimeter(); } //confronto 2 poligoni in base al perimetro
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() > rhs.Perimeter(); }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() <= rhs.Perimeter(); }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() >= rhs.Perimeter(); }

      double computeDistance(const Point& p1, const Point& p2) const{
          return sqrt(pow(p1.X-p2.X,2)+pow(p1.Y-p2.Y,2));                  //pow = fz per fare il quadrato
          }
  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse(): _center(0,0), _a(1), _b(2) { }
      Ellipse(const Point& center,
              const double& a,
              const double& b): _center(center), _a(a), _b(b) { }
      virtual ~Ellipse() { }                                 //distruttore ora vuoto perchè non ho allocato memoria dinamica
      void AddVertex(const Point& point) {_center = point; } //lecito perchè è stato definito costruttore copia alla classe Point
      void SetSemiAxisA(const double& a){ _a = a; }
      void SetSemiAxisB(const double& b){ _b = b; }
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {

    public:
      Circle(): Ellipse() { }
      Circle(const Point& center,
             const double& radius): Ellipse(center,radius,radius) { }
      virtual ~Circle() { }
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle(): points(0) { } //vettore points è vuoto
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point) {points.push_back(point); }
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const double& edge);
  };


  class Quadrilateral : public IPolygon
  {
    protected:
     vector<Point> points;

    public:
     Quadrilateral() {}
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p) { points.push_back(p); }
      double Perimeter() const;
  };


  class Rectangle : public Quadrilateral
  {
    public:
      Rectangle() { }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4) { }
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      virtual ~Rectangle() { }
  };


  class Square: public Rectangle
  {
    public:
      Square() { }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4) {}
      Square(const Point& p1,
             const double& edge);
      virtual ~Square() { }
  };
}

#endif // SHAPE_H
