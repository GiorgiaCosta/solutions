#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      return M_PI*2*sqrt((_a*_a+_b*_b)/2);
  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);

  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    // points = {p1, p2, p3}
   perimeter += computeDistance(points[0],points[1]);
   perimeter += computeDistance(points[1],points[2]);
   perimeter += computeDistance(points[0],points[2]);
   return perimeter;
  }


  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)
  : Triangle(p1, Point(edge+p1.X, p1.Y), Point(p1.X+(edge/2), (sqrt(3)/2)*edge+p1.Y))  //costruttore clase base
  {

  }


  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      points.clear();
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    perimeter += computeDistance(points[0],points[1]);
    perimeter += computeDistance(points[1],points[2]);
    perimeter += computeDistance(points[2],points[3]);
    perimeter += computeDistance(points[0],points[3]);
    return perimeter;
  }


  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height) : Quadrilateral(p1, Point(p1.X+base, p1.Y), Point(p1.X + base, p1.Y+height), Point(p1.X,p1.Y+height))
  {

  }

  Square::Square(const Point &p1, const double &edge) : Rectangle(p1, edge, edge)
  {

  }

  Point Point::operator+(const Point& point) const
  {
      // Point p = p1 + p2;
      Point p; // x = 0, y= 0
      p.X = X + point.X;
      p.Y = Y+point.Y;
      return p;
  }

  //point parametro è il secondo operando
  Point Point::operator-(const Point& point) const
  {
      Point p;
      p.X = X - point.X;
      p.Y = Y - point.Y;
      return p;

  }

  Point&Point::operator-=(const Point& point)
  {
      X = X - point.X;
      Y -= point.Y;
      return *this; // this è un puntatore, per ritornare reference devo ritornare l'oggetto (lo ottengo derefenziando this (cioè *this))

  }

  Point&Point::operator+=(const Point& point)
  {
      X = X + point.X;
      Y += point.Y;
      return *this;

  }


}
