class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.ingr = []


    def addIngredient(self, ingredient: Ingredient):
        self.ingr.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.ingr)

    def computePrice(self) -> int:
        price: int = 0
        for i in range(0, len(self.ingr)):
            price += self.ingr[i].Price

        return price


class Order:

    def __init__(self):
        self.ordine = []
        self.num_order = int

    def getPizza(self, position: int) -> Pizza:
        if position < 1 or position > len(self.ordine):
            raise Exception("Position passed is wrong")

        return self.ordine[position - 1]

    def initializeOrder(self, numPizzas: int):
        self.ordine = [None] * numPizzas

    def addPizza(self, pizza: Pizza):
        self.ordine.append(pizza)

    def numPizzas(self) -> int:
        return len(self.ordine)

    def computeTotal(self) -> int:
        total_price: int = 0
        for i in range(0, len(self.ordine)):
            total_price += self.ordine[i].computePrice()

        return total_price


class Pizzeria:
    def __init__(self):
        self.ordered_ingredient = []
        self.ingredients_in_kitchen = []
        self.menu_ = []
        self.orders = []

    def addIngredient(self, name: str, description: str, price: int):
        for ingredient in self.ingredients_in_kitchen: #scorro ingredienti
            if ingredient.Name == name:
                raise ValueError("Ingredient already inserted")

        new_ingredient = Ingredient(name, price, description)
        self.ingredients_in_kitchen.append(new_ingredient)


    def findIngredient(self, name: str) -> Ingredient:
        for ingredient in self.ingredients_in_kitchen:
            if ingredient.Name == name:
                return ingredient

        raise Exception("Ingredient not found")


    def addPizza(self, name: str, ingredients: []):
        for pizza in self.menu_:   # scorro le pizze
            if pizza.Name == name:
                raise Exception("Pizza already inserted")

        new_pizza = Pizza(name)
        
        #per aggiungere gli ingredienti scorro la lista di input e la confronto con gli elementi della cucina
        for ing in ingredients:
            for ingredient in self.ingredients_in_kitchen:
                if ing == ingredient.Name:
                    new_pizza.ingr.append(ingredient)
        self.menu_.append(new_pizza)


    def findPizza(self, name: str) -> Pizza:
        for pizza in self.menu_:
            if pizza.Name == name:
                return pizza

        raise Exception("Pizza not found")


    def createOrder(self, pizzas: []) -> int:
        if len(pizzas) == 0:
            raise Exception("Empty order")

        new_order = Order()
        for i in range(0, len(pizzas)):
            for pizza in self.menu_:
                if pizzas[i] == pizza.Name:
                    new_order.ordine.append(pizza)

        new_order.num_order = 1000 + len(self.orders)
        self.orders.append(new_order)
        return new_order.num_order


    def findOrder(self, numOrder: int) -> Order:
        for order in self.orders:    #scorro la lista di ordini
            if order.num_order == numOrder:
                return order

        raise Exception("Order not found")


    def getReceipt(self, numOrder: int) -> str:
        for order in self.orders:
            if order.num_order == numOrder:
                s = ""   #str
                for pizza in order.ordine:
                    s += "- " + pizza.Name + ", " + str(pizza.computePrice()) + " euro\n"
                s += "  TOTAL: " + str(order.computeTotal()) + " euro\n"
                return s

        raise Exception("Order not found")


    def listIngredients(self) -> str:
        s = ""    #str   lista di ingredienti
        ingr_ordinealfabetico = []
        for ingredients in self.ingredients_in_kitchen:
            ingr_ordinealfabetico.append(ingredients.Name)
        ingr_ordinealfabetico.sort()    #riordino alfabeticamente

        for t in ingr_ordinealfabetico:
            for ingredients in self.ingredients_in_kitchen:
                if t == ingredients.Name:
                    self.ordered_ingredient.append(ingredients)   #memorizzo in ordered_ingredient gli ingredienti ordinati per nome

        for i in range(0, len(self.ordered_ingredient)):  #formo la lista degli ingredienti
            s += self.ordered_ingredient[i].Name + " - '" + self.ordered_ingredient[i].Description + "': " + str(
                self.ordered_ingredient[i].Price) + " euro\n"
        return s


    def menu(self) -> str:
        s = ""
        for pizza in self.menu_:   #scorro le pizze nel menu
            s += pizza.Name + " (" + str(pizza.numIngredients()) + " ingredients): " + str(
                pizza.computePrice()) + " euro\n"

        return s