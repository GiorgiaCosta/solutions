#include "Pizzeria.h"
#include <exception>

namespace PizzeriaLibrary {
void Pizza::AddIngredient(const Ingredient &ingredient)
{
    ingr.push_back(ingredient);
}

int Pizza::NumIngredients() const
{
    return ingr.size();
}

int Pizza::ComputePrice() const
{
    int price = 0;
    for (unsigned int i = 0; i < ingr.size(); i++){
         price += ingr[i].Price;
     }
     return price;
}

void Order::InitializeOrder(int numPizzas)
{
    ordine.reserve(numPizzas);
}

void Order::AddPizza(const Pizza &pizza)
{
    ordine.push_back(pizza);
}

const Pizza &Order::GetPizza(const unsigned int &position) const
{
    if(position < 1 || position > ordine.size())
        throw "Position passed is wrong";

    return ordine[position-1];
}

int Order::NumPizzas() const
{
    return ordine.size();
}

int Order::ComputeTotal() const
{
    int total_price = 0;
    for(unsigned int i = 0; i < ordine.size(); i++)
        total_price += ordine[i].ComputePrice();

    return total_price;
}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    if(ingredients.find(name)!=ingredients.end())
        throw runtime_error("Ingredient already inserted");

    Ingredient i;
    i.Name = name;
    i.Description = description;
    i.Price=price;

    ingredients.insert({name,i});

}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    if(ingredients.find(name) == ingredients.end())
        throw runtime_error("Ingredient not found");

    return ingredients.find(name)->second;
    // ingredients.find(name)ritorna un pair (first e second)
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    if(menu.find(name)!=menu.end())
        throw runtime_error("Pizza already inserted");

    Pizza p;
    p.Name = name;
    p.ingr.resize(0);

    //riempimento vettore ingr
    for(unsigned int i = 0; i < ingredients.size(); i++){
        Ingredient tmp = Pizzeria::ingredients.find(ingredients[i])->second;
        p.ingr.push_back(tmp);
    }
    menu.insert({name,p});

}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    if(menu.find(name) == menu.end())
        throw runtime_error("Pizza not found");

    return menu.find(name)->second;

}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    if(pizzas.empty())
        throw std::runtime_error("Empty order");

    Order o;
    o.ordine.resize(0);

    //riempimento del vettore "ordine" con pizzas
    for(unsigned int i = 0; i < pizzas.size(); i++){
        Pizza tmp = menu.find(pizzas[i])->second;
        o.AddPizza(tmp);
    }

    o.numOrder = numOrder++;
    orders.insert({o.numOrder,o});

    return o.numOrder;

}

const Order &Pizzeria::FindOrder(const int &numOrder)
{
    if(orders.find(numOrder) == orders.end())
        throw runtime_error("Order not found");

    return orders.find(numOrder)->second;
}

Pizzeria::Pizzeria(): numOrder(1000){   //costruttore

}

string Pizzeria::GetReceipt(const int &numOrder)
{
    if(orders.find(numOrder) == orders.end())
        throw runtime_error("Order not found");

    string s;
    Order o = orders.find(numOrder)->second;
    int total = 0;

    for(const auto &x : o.ordine){ //variabile iteratore
       s.append("- ");
       s.append(x.Name);
       s.append(", ");
       s.append(to_string(x.ComputePrice())); //converto int a stringa con to_string
       s.append(" euro\n");
       total += x.ComputePrice();
       //cout << s
    }

    s.append("  TOTAL: ").append(to_string(total)).append(" euro\n");

    return s;
}


string Pizzeria::ListIngredients() const
{
    string s;

    for(const auto &x : ingredients){
       Ingredient tmp = x.second;
       s.append(tmp.Name);
       s.append(" - '");
       s.append(tmp.Description);
       s.append("': ");
       s.append(to_string(tmp.Price));
       s.append(" euro\n");
       //cout << s
    }

    return s;
}

string Pizzeria::Menu() const
{
    string a;
    for(const auto& x : menu){
       Pizza tmp = x.second;
       a.append(tmp.Name);
       a.append(" (");
       a.append(to_string(tmp.NumIngredients()));
       a.append(" ingredients): ");
       a.append(to_string(tmp.ComputePrice()));
       a.append(" euro\n");
    }

    return a;
}
}
