#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <string.h>
#include <unordered_set>
#include <algorithm>
#include <map>
#include <unordered_map>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
  };

  class Pizza {
  public:
      vector<Ingredient> ingr;
      string Name;

      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const;
      int ComputePrice() const;
  };

  class Order {
  public:
      vector<Pizza> ordine;
      int numOrder;

      void InitializeOrder(int numPizzas);
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const unsigned int& position) const;
      int NumPizzas() const;
      int ComputeTotal() const;
  };

  class Pizzeria {
  public:
      map<string,Ingredient> ingredients;
      unordered_map<string,Pizza> menu;
      int numOrder;
      map<int,Order> orders;

    public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder);
      string GetReceipt(const int& numOrder);
      string ListIngredients() const;
      string Menu() const;
      Pizzeria();        //costruttore
  };
};

#endif // PIZZERIA_H
