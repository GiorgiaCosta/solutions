#ifndef VIAMICHELIN_H
#define VIAMICHELIN_H
#include <sstream>
#include <iostream>
#include <exception>

#include "bus.h"
#include <vector>
#include <fstream>
using namespace std;
using namespace BusLibrary;

namespace ViaMichelinLibrary {

  class BusStation : public IBusStation {
  private:
      vector<Bus> _bus;
      int _numberBus;
      string _busFilePath;

    public:
      BusStation(const string& busFilePath) { _busFilePath = busFilePath;}
      void Load();
      int NumberBuses() const  { return  _numberBus;}
      const Bus& GetBus(const int& idBus) const;
  };

  class MapData : public IMapData {
  private:
      string _mapFilePath;
      int _numberBusStops = 0;
      int _numberStreets = 0;
      int _numberRoutes = 0;
      vector<BusStop> _busStops;
      vector<Street> _streets;
      vector<Route> _routes;
      //aggiunti dopo
      vector<int> _streetsFrom;
      vector<int> _streetsTo;
      vector<vector<int>> _routeStreets;

      void Reset();

    public:
      MapData(const string& mapFilePath) { _mapFilePath = mapFilePath;}
      void Load();
      int NumberRoutes() const { return _numberRoutes;}
      int NumberStreets() const { return _numberStreets;}
      int NumberBusStops() const { return _numberBusStops; }
      const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const;
      const Route& GetRoute(const int& idRoute) const;

      const Street& GetStreet(const int& idStreet) const;
      const BusStop& GetStreetFrom(const int& idStreet) const;
      const BusStop& GetStreetTo(const int& idStreet) const ;

      const BusStop& GetBusStop(const int& idBusStop) const;
  };

  class RoutePlanner : public IRoutePlanner {
  public:
      static int BusAverageSpeed;
  private:
       const IMapData& _mapData;
       const IBusStation& _busStation;

  public:
      RoutePlanner(const IMapData& mapData,
                   const IBusStation& busStation) : _mapData(mapData), _busStation(busStation) { }

      int ComputeRouteTravelTime(const int& idRoute) const;
      int ComputeRouteCost(const int& idBus, const int& idRoute) const;
  };

  class MapViewer : public IMapViewer {
  private:
        const IMapData& _mapData;
    public:
      MapViewer(const IMapData& mapData) : _mapData(mapData) { }
      string ViewRoute(const int& idRoute) const;
      string ViewStreet(const int& idStreet) const;
      string ViewBusStop(const int& idBusStop) const;
  };
}

#endif // VIAMICHELIN_H
