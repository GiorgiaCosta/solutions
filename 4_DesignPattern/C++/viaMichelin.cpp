# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Load()
{
    ///reset station
    _numberBus = 0;
    _bus.clear();

    ///open file
    ifstream fd;
    fd.open(_busFilePath,ifstream::in); //apertura in lettura
    if(fd.fail())   //!fd.is_open()
        throw runtime_error("Something goes wrong");


    ///load buses
    try {
    string line;
    getline(fd,line);          //leggo prima riga (ma non mi serve) e memorizzo in line
    getline(fd, line);

    istringstream convertN;    //converto in int
    convertN.str(line);
    convertN >> _numberBus;

    getline(fd,line);
    getline(fd,line);

     while(getline(fd,line)){
         istringstream s(line);
         Bus tmp;
         string str;
         s >> str;
         tmp.Id = stoi(str);

         s >> str;
         tmp.FuelCost = stoi(str);

         _bus.push_back(tmp);
      }

     ///close file
     fd.close();

    } catch(exception){

        _bus.clear();
        throw runtime_error("Something goes wrong");
    }
}

const Bus &BusStation::GetBus(const int &idBus) const
{
    if( idBus < 1 || idBus > _numberBus )
          throw runtime_error("Bus " + to_string(idBus)+ " does not exists");
    return _bus[idBus-1];

}

void MapData::Reset()
  {
    _numberRoutes = 0;
    _numberStreets = 0;
    _numberBusStops = 0;
    _busStops.clear();
    _streets.clear();
    _routes.clear();
    _streetsFrom.clear();
    _streetsTo.clear();
    _routeStreets.clear();
  }

void MapData::Load()
{
    /// Reset map
    Reset();

    ///open file
    ifstream fd;
    fd.open(_mapFilePath,ifstream::in);

    if(!fd.is_open())
        throw runtime_error("Something goes wrong");

    ///load
    string line;

    /// Get busStops
    getline(fd,line);
    getline(fd,line);
    istringstream convertBusStops;
    convertBusStops.str(line);
    convertBusStops >> _numberBusStops;

    getline(fd,line);

    for(int i = 0; i < _numberBusStops; i++){
        string str;
        getline(fd,line); //line contiene  1 Cadorna 450781 76761
        istringstream s(line);
        BusStop tmp;
        s >> str;
        tmp.Id = stoi(str);
        s >> str;
        tmp.Name = str;
        s >> str;
        tmp.Latitude = stoi(str);
        s >> str;
        tmp.Longitude = stoi(str);
        _busStops.push_back(tmp);
    }

    /// Get streets
  getline(fd,line);
  getline(fd,line);
  istringstream convertStreets;
  convertStreets.str(line);
  convertStreets >> _numberStreets;

  getline(fd,line);


  for(int i = 0; i < _numberStreets; i++){
      string str;
      getline(fd,line);
      istringstream s(line);
      Street tmp;
      s >> str;
      tmp.Id = stoi(str);
      s >> str;
      tmp.From = stoi(str);
      s >> str;
      tmp.To = stoi(str);
      s >> str;
      tmp.TravelTime = stoi(str);
      _streets.push_back(tmp);
  }

  /// Get routes
  getline(fd,line);
  getline(fd,line);
  istringstream convertRoutes;
  convertRoutes.str(line);
  convertRoutes >> _numberRoutes;

  getline(fd,line);
  for(int i = 0; i < _numberRoutes; i++){
      string str;
      Route tmp;
      tmp.StreetIds.resize(0);

      getline(fd,line);

      istringstream s(line);
      s >> str;
      tmp.Id = stoi(str);
      s >> str;
      tmp.NumberStreets = stoi(str);
      for(int j = 0; j < tmp.NumberStreets; j++){
          s >> str;
          tmp.StreetIds.push_back(stoi(str));
      }
      _routes.push_back(tmp);
  }

  /// Close File
  fd.close();
}


const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
{
    if (idRoute > _numberRoutes)
          throw runtime_error("Route " + to_string(idRoute) + " does not exists");
    const Route& route = _routes[idRoute - 1];

    if (streetPosition >= route.NumberStreets)
        throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

    int idStreet = _routeStreets[idRoute - 1][streetPosition];

    return _streets[idStreet - 1];
}

const Route &MapData::GetRoute(const int &idRoute) const
{
    if (idRoute > _numberRoutes)
         throw runtime_error("Route " + to_string(idRoute) + " does not exists");

       return _routes[idRoute - 1];
}

const Street &MapData::GetStreet(const int &idStreet) const
{
    if (idStreet > _numberStreets)
          throw runtime_error("Street " + to_string(idStreet) + " does not exists");

        return _streets[idStreet - 1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const
{
    if (idStreet > _numberStreets)
          throw runtime_error("Street " + to_string(idStreet) + " does not exists");

        int idFrom = _streetsFrom[idStreet - 1];

        return _busStops[idFrom - 1];
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const
{
    if (idStreet > _numberStreets)
         throw runtime_error("Street " + to_string(idStreet) + " does not exists");

       int idTo = _streetsTo[idStreet - 1];

       return _busStops[idTo - 1];
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const
{
    if (idBusStop > _numberBusStops)
          throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");

    return _busStops[idBusStop - 1];
}


int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);

    int travelTime = 0;
    for (int s = 0; s < route.NumberStreets; s++)
        travelTime += _mapData.GetRouteStreet(idRoute, s).TravelTime;

    return travelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);
    double totalTravelTime = 0;

    for (int s = 0; s < route.NumberStreets; s++)
          totalTravelTime += _mapData.GetRouteStreet(idRoute, s).TravelTime * _busStation.GetBus(idBus).FuelCost * BusAverageSpeed;


    return totalTravelTime/(60*60);
}

string MapViewer::ViewRoute(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);
    int s = 0;

    ostringstream routeView;
    routeView<< to_string(idRoute)<< ": ";

    for (s= 0; s < route.NumberStreets - 1; s++)
    {
        int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
         string from = _mapData.GetStreetFrom(idStreet).Name;
         routeView << from<< " -> ";
     }

    int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
    string from = _mapData.GetStreetFrom(idStreet).Name;
    string to = _mapData.GetStreetTo(idStreet).Name;
    routeView << from<< " -> "<< to;

    return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const
{
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
        const BusStop& to = _mapData.GetStreetTo(idStreet);

        return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const
{
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

        return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")";
}

}
